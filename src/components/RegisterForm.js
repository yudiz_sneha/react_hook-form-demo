import React from "react";
import {
  TextField,
  FormControlLabel,
  Checkbox,
  FormLabel,
  FormControl,
  FormHelperText,
  RadioGroup,
  Radio,
  InputLabel,
  Select,
  MenuItem,
  Button,
} from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import { useForm, Controller } from "react-hook-form";

const useStyles = makeStyles((theme) => ({
  inputField: {
    width: "100%",
    margin: theme.spacing(1, 0),
  },
}));

const RegisterForm = () => {
  const classes = useStyles();
  const { register, handleSubmit, control, errors } = useForm();
  const onSubmit = (data) => console.log(data);
  console.log(errors);

  return (
    <div className="box">
      <div className="box-primary">
      <h1 style={{textAlign: "center", fontFamily: "cursive", fontSize: "50px"}}>Registration Form</h1>
      </div>

      <div className="box-secondary">
        <form onSubmit={handleSubmit(onSubmit)}>

         {/* TextField - Firstname */}
          <TextField
            placeholder="Enter Your Firstname"
            label="Enter Your Firstname"
            variant="outlined"
            fullWidth
            className={classes.inputField}
            name="firstName"
            inputRef={register({
              required: "Firstname is required.",
            })}
            error={Boolean(errors.firstName)}
            helperText={errors.firstName?.message}
          />

          {/* TextField - Lastname */}
          <TextField
            placeholder="Enter Your Lastname"
            label="Enter Your Lastname"
            variant="outlined"
            fullWidth
            className={classes.inputField}
            name="lastName"
            inputRef={register({
              required: "Lastname is required.",
            })}
            error={Boolean(errors.lastName)}
            helperText={errors.lastName?.message}
          />

          {/* TextField - Email */}
          <TextField
            placeholder="Enter Your E-mail ID"
            label="Enter Your E-mail ID"
            variant="outlined"
            fullWidth
            className={classes.inputField}
            name="email"
            inputRef={register({
              required: "E-mail ID is required.",
            })}
            error={Boolean(errors.email)}
            helperText={errors.email?.message}
          />

          {/* TextField - Phone-number */}
          <TextField
            placeholder="Enter Your Phone Number"
            label="Enter Your Phone Number"
            variant="outlined"
            fullWidth
            className={classes.inputField}
            name="phone"
            inputRef={register({
              required: "Phone Number is required.",
            })}
            error={Boolean(errors.phone)}
            helperText={errors.phone?.message}
          />

          {/* Gender */}
          {/* Female */}
          <FormControl
            className={classes.inputField}
            error={Boolean(errors.gender)}
          >
            <FormLabel>Choose Your Gender</FormLabel>
            <RadioGroup row name="gender">
              <FormControlLabel
                value="female"
                control={
                  <Radio
                    inputRef={register({
                      required: "Choose your Gender",
                    })}
                  />
                }
                label="Female"
              />

              {/* Male */}
              <FormControlLabel
                value="male"
                control={
                  <Radio
                    inputRef={register({
                      required: "Choose your Gender",
                    })}
                  />
                }
                label="Male"
              />
            </RadioGroup>
            <FormHelperText>{errors.gender?.message}</FormHelperText>
          </FormControl>

          {/* Select */}
          <FormControl
            fullWidth
            className={classes.inputField}
            error={Boolean(errors.course)}
          >
            <InputLabel id="demo-simple-select-label">
              Select Your Department
            </InputLabel>

            <Controller
              render={(props) => (
                <Select value={props.value} onChange={props.onChange}>
                  <MenuItem value="MERN Stack">MERN Stack</MenuItem>
                  <MenuItem value="MEAN Stack">MEAN Stack</MenuItem>
                  <MenuItem value="PHP">PHP</MenuItem>
                </Select>
              )}
              name="course"
              control={control}
              defaultValue=""
              rules={{
                required: "Please select your department.",
              }}
            />
            <FormHelperText>{errors.course?.message}</FormHelperText>
          </FormControl>

          {/* Terms and Conditions */}
          <FormControl
            error={Boolean(errors.tnc)}
            style={{ display: "block", marginBottom: 15 }}
          >
            <FormControlLabel
              control={
                <Checkbox
                  name="tnc"
                  inputRef={register({
                    required: "Please agree our Terms & Conditions",
                  })}
                />
              }
              label="I agree all the Terms & Conditions"
            />
            <FormHelperText>{errors.tnc?.message}</FormHelperText>
          </FormControl>

          <Button variant="contained" color="primary" type="submit">
            Create new account
          </Button>
        </form>
      </div>
    </div>
  );
};

export default RegisterForm;
